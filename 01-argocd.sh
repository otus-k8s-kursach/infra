kubectl create namespace argocd
cd ./argocd/charts/argo-cd
helm upgrade --install argocd . -f values.yaml -n argocd --wait
helm upgrade --install argocd . -f values.yaml -f values-repo.yaml -f values-proj.yaml -f values-app.yaml -n argocd --wait
cd ../.. && kubectl -n argocd apply -f ingress.yaml