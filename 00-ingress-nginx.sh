kubectl create ns ingress-nginx
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install -ningress-nginx ingress-nginx ingress-nginx/ingress-nginx

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.9.1/cert-manager.yaml
kubectl apply -f ./ingress-nginx/acme-issuer.yaml
