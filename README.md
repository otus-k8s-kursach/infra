Поднимаем Managed Cluster в Yandex Cloud
../k8s-yc/create.sh

Создаем DNS запись *.yc.ga66a.ru на  IP балансировщика

Поднимаем Ingress Controller Nginx и Cert Manager
./00-ingress.nginx.sh

Поднимаем ArgoCD и создаем для него Ingress
./01-argocd.sh
Далее argocd доступен по адресу:
https://argocd.yc.ga66a.ru/
Пароль от админки:
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d


Все остальные приложения поднимаются через ArgoCD.


Мониторинг: Prometheus, promtail, loki, grafana
Repo: https://gitlab.com/otus-k8s-kursach/infra.git
Path: ./loki-stack/
URL: https://grafana.yc.ga66a.ru/
Login: admin
Password: kubectl -n grafana get secret loki-stack-charts-grafana -o jsonpath="{.data.admin-password}" | base64 -d

В графану добавляем datasources:
 - prometheus http://loki-stack-charts-prometheus-server.prometheus/
 - loki http://loki-stack-charts.loki:3100/

Импортируем дашборды: 315, 13332, 14055, 13639, 15196


NEXUS
Repo: https://gitlab.com/otus-k8s-kursach/infra.git
Path: /nexus/
Url: https://nexus.yc.ga66a.ru/
Начальный пароль от админки Nexus: kubectl exec -n nexus $(kubectl get po -n nexus | grep nexus | awk '{ print $1 }') -- cat /nexus-data/admin.password

Сервис и ингресс для Docker Registry создается в ArgoCD, а вот сам registry создаем ручками через админку на 5000 порту.
Т.ж. создаем роли и пользователей для чтения и записи в реджестри
Адрес реджестри: https://registry.yc.ga66a.ru/

